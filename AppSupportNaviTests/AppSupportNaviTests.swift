//
//  AppSupportNaviTests.swift
//  AppSupportNaviTests
//
//  Created by chephes on 1/26/23.
//

import XCTest
@testable import AppSupportNavi
import SwiftASN1

final class AppSupportNaviTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testDecodeBasic4Info() throws {
        let data = Data([0xFF, 0x20, 0x4D, 0xDF, 0x21, 0x08, 0x00, 0x0E, 0x00, 0x20, 0x00, 0x59, 0x00, 0x64, 0xDF, 0x22, 0x0F, 0xE7, 0x94, 0xB3, 0xE8, 0xAB, 0x8B, 0xE3, 0x80, 0x80, 0xE5, 0xA4, 0xAA, 0xE9, 0x83, 0x8E, 0xDF, 0x23, 0x1E, 0xE6, 0x9D, 0xB1, 0xE4, 0xBA, 0xAC, 0xE9, 0x83, 0xBD, 0xE5, 0x8D, 0x83, 0xE4, 0xBB, 0xA3, 0xE7, 0x94, 0xB0, 0xE5, 0x8C, 0xBA, 0xEF, 0xBC, 0x91, 0xE2, 0x88, 0x92, 0xEF, 0xBC, 0x91, 0xDF, 0x24, 0x08, 0x31, 0x39, 0x39, 0x35, 0x30, 0x31, 0x30, 0x33, 0xDF, 0x25, 0x01, 0x31])
        let length = Int(data[2]) + 3
        let node = try DER.parse(Array(data.prefix(length)))
        let basic4Info = try Basic4Info(derEncoded: node)
        XCTAssertEqual(basic4Info, Basic4Info(name: "申請　太郎", address: "東京都千代田区１−１", birthdate: "19950103", genderCode: "1"))
    }

    func testExample() throws {
        let data = Data([0x11, 0x01, 0x78, 0x12, 0x08, 0x36, 0x61, 0x46, 0x23, 0x21, 0x21, 0x40, 0x3F, 0x13, 0x10, 0x25, 0x33, 0x25, 0x73, 0x25, 0x49, 0x25, 0x26, 0x21, 0x21, 0x25, 0x5E, 0x25, 0x33, 0x25, 0x48, 0x14, 0x00, 0x15, 0x10, 0x25, 0x33, 0x25, 0x73, 0x25, 0x55, 0x25, 0x37, 0x25, 0x3B, 0x25, 0x24, 0x21, 0x21, 0x21, 0x21, 0x16, 0x07, 0x33, 0x36, 0x32, 0x30, 0x36, 0x32, 0x35, 0x17, 0x22, 0x40, 0x6E, 0x3A, 0x6A, 0x3B, 0x54, 0x40, 0x6E, 0x3A, 0x6A, 0x36, 0x68, 0x45, 0x4F, 0x45, 0x44, 0x3B, 0x33, 0x32, 0x26, 0x44, 0x2E, 0x23, 0x31, 0x23, 0x36, 0x21, 0x5D, 0x23, 0x36, 0x21, 0x5D, 0x23, 0x34, 0x18, 0x07, 0x35, 0x30, 0x31, 0x30, 0x36, 0x31, 0x30, 0x19, 0x05, 0x30, 0x34, 0x39, 0x37, 0x35, 0x1A, 0x04, 0x4D, 0x25, 0x4E, 0x49, 0x1B, 0x07, 0x35, 0x30, 0x36, 0x30, 0x37, 0x32, 0x35, 0x1C, 0x06, 0x34, 0x63, 0x36, 0x40, 0x45, 0x79, 0x1D, 0x32, 0x3D, 0x60, 0x43, 0x66, 0x37, 0x3F, 0x24, 0x47, 0x31, 0x3F, 0x45, 0x3E, 0x24, 0x47, 0x24, 0x2D, 0x24, 0x6B, 0x3D, 0x60, 0x43, 0x66, 0x37, 0x3F, 0x3C, 0x56, 0x24, 0x4F, 0x3D, 0x60, 0x43, 0x66, 0x37, 0x3F, 0x3C, 0x56, 0x21, 0x4A, 0x23, 0x35, 0x23, 0x74, 0x21, 0x4B, 0x24, 0x4B, 0x38, 0x42, 0x24, 0x6B, 0x1E, 0x26, 0x3D, 0x60, 0x43, 0x66, 0x37, 0x3F, 0x3C, 0x56, 0x21, 0x4A, 0x23, 0x35, 0x23, 0x74, 0x21, 0x4B, 0x24, 0x48, 0x49, 0x61, 0x44, 0x4C, 0x3C, 0x56, 0x24, 0x4F, 0x23, 0x41, 0x23, 0x54, 0x3C, 0x56, 0x24, 0x4B, 0x38, 0x42, 0x24, 0x6B, 0x1F, 0x00, 0x20, 0x12, 0x3F, 0x40, 0x46, 0x60, 0x40, 0x6E, 0x38, 0x29, 0x38, 0x78, 0x30, 0x42, 0x30, 0x51, 0x30, 0x77, 0x32, 0x71, 0x21, 0x0C, 0x34, 0x35, 0x30, 0x39, 0x30, 0x31, 0x39, 0x38, 0x33, 0x32, 0x31, 0x31, 0x22, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x23, 0x07, 0x34, 0x32, 0x31, 0x30, 0x33, 0x32, 0x34, 0x24, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x25, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x26, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x27, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x28, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x29, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x2A, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x2B, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x2C, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x2D, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x2E, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x2F, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x31, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x32, 0x07, 0x35, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x33, 0x07, 0x34, 0x32, 0x31, 0x30, 0x33, 0x32, 0x34, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
        let decodedData = try JPDriversLicenseRecordData(data: data)
        XCTAssertEqual(
            JPDriversLicenseRecordData(
                jisYearNumber: 0x78,
                name: "近藤　誠",
                nameReading: "コンドウ　マコト",
                nickname: "",
                uniformNameReading: "コンフシセイ　　",
                birthdate: "3620625",
                address: "川崎市川崎区渡田山王町１６−６−４",
                deliveryDate: "5010610",
                inquiryNumber: "04975",
                licenseColorClassification: "優良",
                validUntil: "5060725",
                condition1: "眼鏡等",
                condition2: "準中型で運転できる準中型車は準中型車（５ｔ）に限る",
                condition3: "準中型車（５ｔ）と普通車はＡＴ車に限る",
                condition4: "",
                publicSecurityCommitteeName: "神奈川県公安委員会",
                licenseNumber: "450901983211",
                licenseDate2andSmallandMotorbike: "5000000",
                licenseDateOthers: "4210324",
                licenseDateType2: "5000000",
                licenseDateLarge: "5000000",
                licenseDateNormal: "5000000",
                licenseDateLargeSpecial: "5000000",
                licenseDateLargeBike: "5000000",
                licenseDateNormalBike: "5000000",
                licenseDateSmallSpecial: "5000000",
                licenseDateMotorbike: "5000000",
                licenseDateTowing: "5000000",
                licenseDateLarge2: "5000000",
                licenseDateNormal2: "5000000",
                licenseDateLargeSpecial2: "5000000",
                licenseDateTowing2: "5000000",
                licenseDateMiddle: "5000000",
                licenseDateMiddle2: "5000000",
                licenseDateSemiMiddle: "4210324"
            ),
            decodedData,
            "免許証の記載事項を正しくデコードできる"
        )
    }

    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
