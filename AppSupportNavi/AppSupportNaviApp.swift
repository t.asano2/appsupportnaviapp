//
//  AppSupportNaviApp.swift
//  AppSupportNavi
//
//  Created by chephes on 1/26/23.
//

import SwiftUI

@main
struct AppSupportNaviApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
