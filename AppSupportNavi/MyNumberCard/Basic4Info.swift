//
//  Basic4Info.swift
//  AppSupportNavi
//
//  Created by chephes on 4/29/23.
//

import SwiftASN1

struct Basic4Info: Codable, Equatable {
    let name: String;
    let address: String;
    let birthdate: String;
    let genderCode: String;
    
    init(name: String, address: String, birthdate: String, genderCode: String) {
        self.name = name
        self.address = address
        self.birthdate = birthdate
        self.genderCode = genderCode
    }
    
    init(derEncoded: ASN1Node) throws {
        self = try DER.sequence(derEncoded, identifier: ASN1Identifier(tagWithNumber: 0x20, tagClass: .private)) { nodes in
            var name = ""
            var address = ""
            var birthdate = ""
            var genderCode = ""
            while let n = nodes.next() {
                let content = n.content
                switch (content) {
                case let .primitive(content):
                    switch (n.identifier.tagNumber) {
                    case 0x22:
                        name = String(decoding: content, as: UTF8.self)
                        break
                    case 0x23:
                        address = String(decoding: content, as: UTF8.self)
                        break
                    case 0x24:
                        birthdate = String(decoding: content, as: UTF8.self)
                        break
                    case 0x25:
                        genderCode = String(decoding: content, as: UTF8.self)
                        break
                    default:
                        break
                    }
                    break
                default:
                    break
                }
            }
            return Basic4Info(name: name, address: address, birthdate: birthdate, genderCode: genderCode)
        }
    }
}
