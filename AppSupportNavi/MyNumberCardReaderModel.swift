//
//  MyNumberCardReaderModel.swift
//  AppSupportNavi
//
//  Created by chephes on 4/27/23.
//

import Foundation
import CoreNFC
import SwiftASN1
import CryptoKit

class MyNumberCardReaderModel: NSObject, NFCTagReaderSessionDelegate {
    var session: NFCTagReaderSession?
    private var continuation: CheckedContinuation<NFCTag, Error>?
    private var invalidationContinuation: CheckedContinuation<Void, Error>?
    static let EFID_SIGNATURE_PRIVATE_KEY: (UInt8, UInt8) = (0x00, 0x1A)
    static let EFID_SIGNATURE_PASSWORD: (UInt8, UInt8) = (0x00, 0x1B)
    static let EFID_PUBLIC_AUTH_PRIVATE_KEY: (UInt8, UInt8) = (0x00, 0x17)
    static let EFID_PUBLIC_AUTH_PASSWORD: (UInt8, UInt8) = (0x00, 0x18)
    static let SHORT_EFID_SIGNATURE_CERT: UInt8 = 0x01;
    static let SHORT_EFID_PUBLIC_AUTH_CERT: UInt8 = 0x0A;
    static let SHORT_EFID_TOKEN_INFO: UInt8 = 0x06;

    func handleISO7816Tag<T>(
        using processingClosure: @escaping (NFCISO7816Tag) async throws -> T
    ) async throws -> T {
        let tag = try await withCheckedThrowingContinuation({ continuation in
            self.continuation = continuation
            session = NFCTagReaderSession(pollingOption: [.iso14443], delegate: self)
            session?.begin()
            print("begin")
        })
        try await session?.connect(to: tag)
        print("connected")
        var result:T? = nil;
        switch tag {
        case let .iso7816(iso7816Tag):
            do {
                result = try await processingClosure(iso7816Tag)
                try await withCheckedThrowingContinuation({ continuation in
                    self.invalidationContinuation = continuation
                    session?.invalidate()
                })
            } catch let e {
                switch e {
                case MyNumberCardReadError.pinChallengeFailed(let remainingNumberOfAttempts):
                    try await withCheckedThrowingContinuation({ continuation in
                        self.invalidationContinuation = continuation
                        self.session?.invalidate(errorMessage: "PINが間違っています。\nあと\(remainingNumberOfAttempts)回間違えるとカードがロックされます。")
                    })
                    throw e
                case NFCReaderError.readerTransceiveErrorTagResponseError:
                    try await withCheckedThrowingContinuation({ continuation in
                        self.invalidationContinuation = continuation
                        self.session?.invalidate(errorMessage: "マイナンバーカードの読み取りに失敗しました。")
                    })
                    throw e
                default:
                    print("error: \(e)")
                    break
                }
            }
            if let r = result {
                return r
            }
            throw MyNumberCardReadError.unknownError
        default:
            try await withCheckedThrowingContinuation({ continuation in
                self.invalidationContinuation = continuation
                session?.invalidate(errorMessage: "エラーが発生しました")
            })
            throw MyNumberCardReadError.unknownError
        }
    }
    
    func readBasic4Info(_ pinNumber: String) async throws -> Basic4Info {
        return try await handleISO7816Tag { tag in
            self.session?.alertMessage = "マイナンバーカードを読み取り中..."
            try await self.selectCardInfoInputHelper(tag)
            try await self.selectEF(tag, efid: (0x00, 0x11))
            self.session?.alertMessage = "パスワード照合中..."
            try await self.insVerify(tag, pinNumber: pinNumber)
            try await self.selectEF(tag, efid: (0x00, 0x02))
            self.session?.alertMessage = "基本4情報読み取り中..."
            let data = try await self.readBinary(tag, offset: 0x0)
            print(data.map { String(format:"0x%02X", $0) }.joined(separator: ", "))
            let length = Int(data[2]) + 3 // TODO: 長さが長い場合がある
            self.session?.alertMessage = "データ変換中..."
            let node = try DER.parse(Array(data.prefix(length)))
            let basic4Info = try Basic4Info(derEncoded: node)
            return basic4Info
        }
    }
    
    func readToken() async throws -> String {
        return try await handleISO7816Tag { tag in
            self.session?.alertMessage = "マイナンバーカードを読み取り中..."
            try await self.selectJPKI(tag)
            let data = try await self.readBinary(tag, efId: MyNumberCardReaderModel.SHORT_EFID_TOKEN_INFO, offset: 0x0, length: 0x20);
            return String(data: data, encoding: .ascii)!
        }
    }
    
    func readCert(tag: NFCISO7816Tag, efId: UInt8) async throws -> Data {
        let certHead = try await self.readBinary(tag, efId: efId, offset:0x0, length: 0x04)
        let certRest = try await self.readBinary(tag, efId: efId, offset: 0x4, length: (certHead[2], certHead[3]))
        return certHead + certRest
    }
    
    func readPublicAuthCertificate() async throws -> Data {
        return try await handleISO7816Tag { tag in
            self.session?.alertMessage = "マイナンバーカードを読み取り中..."
            try await self.selectJPKI(tag)
            self.session?.alertMessage = "利用者証明用証明書を読み取り中..."
            return try await self.readCert(tag: tag, efId: MyNumberCardReaderModel.SHORT_EFID_PUBLIC_AUTH_CERT)
        }
    }
    
    func readDigitalSignatureCertificate(_ pinNumber: String) async throws -> Data {
        return try await handleISO7816Tag { tag in
            self.session?.alertMessage = "マイナンバーカードを読み取り中..."
            try await self.selectJPKI(tag)
            self.session?.alertMessage = "署名用パスワード照合中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_SIGNATURE_PASSWORD)
            try await self.insVerify(tag, pinNumber: pinNumber)
            self.session?.alertMessage = "署名用証明書を読み取り中..."
            return try await self.readCert(tag: tag, efId: MyNumberCardReaderModel.SHORT_EFID_SIGNATURE_CERT)
        }
    }
    
    func signWithPublicAuthKey(_ pinNumber: String, hashAlgorithm: HashAlgorithm, base64Hash: String) async throws -> (Data, Data) {
        return try await handleISO7816Tag { tag in
            self.session?.alertMessage = "マイナンバーカードを読み取り中..."
            try await self.selectJPKI(tag)
            self.session?.alertMessage = "利用者証明用証明書を読み取り中..."
            let cert = try await self.readCert(tag: tag, efId: MyNumberCardReaderModel.SHORT_EFID_PUBLIC_AUTH_CERT)
            self.session?.alertMessage = "利用者証明用PIN照合中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_PUBLIC_AUTH_PASSWORD)
            try await self.insVerify(tag, pinNumber: pinNumber)
            self.session?.alertMessage = "利用者証明用秘密鍵による署名作成中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_PUBLIC_AUTH_PRIVATE_KEY)
            let data = try await self.computeDigitalSignature(tag, hashAlgorithm: hashAlgorithm, hash: Data(base64Encoded: base64Hash)!);
            return (cert, data)
        }
    }

    func signWithDigitalSignatureKey(_ pinNumber: String, hashAlgorithm: HashAlgorithm, base64Hash: String) async throws -> (Data, Data) {
        return try await handleISO7816Tag { tag in
            self.session?.alertMessage = "マイナンバーカードを読み取り中..."
            try await self.selectJPKI(tag)
            self.session?.alertMessage = "署名用パスワード照合中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_SIGNATURE_PASSWORD)
            try await self.insVerify(tag, pinNumber: pinNumber)
            self.session?.alertMessage = "署名用証明書を読み取り中..."
            let cert = try await self.readCert(tag: tag, efId: MyNumberCardReaderModel.SHORT_EFID_SIGNATURE_CERT)
            self.session?.alertMessage = "署名用秘密鍵による署名作成中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_SIGNATURE_PRIVATE_KEY)
            let data = try await self.computeDigitalSignature(tag, hashAlgorithm: hashAlgorithm, hash: Data(base64Encoded: base64Hash)!);
            return (cert, data)
        }
    }
    
    func signWithPublicAuthKeyRaw(_ pinNumber: String, base64Data: String) async throws -> (Data, Data) {
        return try await handleISO7816Tag { tag in
            self.session?.alertMessage = "マイナンバーカードを読み取り中..."
            try await self.selectJPKI(tag)
            self.session?.alertMessage = "利用者証明用証明書を読み取り中..."
            let cert = try await self.readCert(tag: tag, efId: MyNumberCardReaderModel.SHORT_EFID_PUBLIC_AUTH_CERT)
            self.session?.alertMessage = "利用者証明用パスワード照合中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_PUBLIC_AUTH_PASSWORD)
            try await self.insVerify(tag, pinNumber: pinNumber)
            self.session?.alertMessage = "利用者証明用秘密鍵による署名作成中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_PUBLIC_AUTH_PRIVATE_KEY)
            let data = try await self.computeDigitalSignatureRaw(tag, raw: Data(base64Encoded: base64Data)!);
            return (cert, data)
        }
    }

    func signWithDigitalSignatureKeyRaw(_ pinNumber: String, base64Data: String) async throws -> (Data, Data) {
        return try await handleISO7816Tag { tag in
            self.session?.alertMessage = "マイナンバーカードを読み取り中..."
            try await self.selectJPKI(tag)
            self.session?.alertMessage = "署名用パスワード照合中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_SIGNATURE_PASSWORD)
            try await self.insVerify(tag, pinNumber: pinNumber)
            self.session?.alertMessage = "署名用証明書を読み取り中..."
            let cert = try await self.readCert(tag: tag, efId: MyNumberCardReaderModel.SHORT_EFID_SIGNATURE_CERT)
            self.session?.alertMessage = "署名用秘密鍵による署名作成中..."
            try await self.selectEF(tag, efid: MyNumberCardReaderModel.EFID_SIGNATURE_PRIVATE_KEY)
            let data = try await self.computeDigitalSignatureRaw(tag, raw: Data(base64Encoded: base64Data)!);
            return (cert, data)
        }
    }
    
    func selectCardInfoInputHelper(_ iso7816Tag: NFCISO7816Tag) async throws {
        let _ = try await iso7816Tag.apduCommand(data: Data([0x00, 0xA4, 0x04, 0x0C, 0x0A, 0xD3, 0x92, 0x10, 0x00, 0x31, 0x00, 0x01, 0x01, 0x04, 0x08])) { _ in
            throw MyNumberCardReadError.notImplemented(ap: .CardInfoInputHelper)
        }
    }
    
    func selectJPKI(_ iso7816Tag: NFCISO7816Tag) async throws {
        let _ = try await iso7816Tag.apduCommand(data: Data([0x00, 0xA4, 0x04, 0x0C, 0x0A, 0xD3, 0x92, 0xF0, 0x00, 0x26, 0x01, 0x00, 0x00, 0x00, 0x01])) { _ in
            throw MyNumberCardReadError.notImplemented(ap: .JPKI)
        }
    }
    
    func selectEF(_ iso7816Tag: NFCISO7816Tag, efid: (UInt8, UInt8)) async throws {
        let _ = try await iso7816Tag.apduCommand(data: Data([0x00, 0xA4, 0x02, 0x0C, 0x02, efid.0, efid.1])) { _ in
            throw MyNumberCardReadError.unknownError
        }
    }
    
    func insVerify(_ iso7816Tag: NFCISO7816Tag, pinNumber: String) async throws {
        let asciiArray: [UInt8] = pinNumber.utf8.map { $0 }
        let count = asciiArray.count
        let _ = try await iso7816Tag.apduCommand(data: Data([0x00, 0x20, 0x00, 0x80, UInt8(count)] + asciiArray.prefix(count))) { result in
            if result.1 == 0x63 {
                throw MyNumberCardReadError.pinChallengeFailed(remainingNumberOfAttempts: result.2 - 0xc0)
            } else {
                throw MyNumberCardReadError.unknownError
            }
        }
    }
    
    func readBinary(_ iso7816Tag: NFCISO7816Tag, offset: UInt8) async throws -> Data {
        let result = try await iso7816Tag.apduCommand(data: Data([0x00, 0xB0, 0x00, offset, 0x00])) { result in
            throw MyNumberCardReadError.unknownError
        }
        return result.0
    }
    
    func readBinary(_ iso7816Tag: NFCISO7816Tag, efId: UInt8, offset: UInt8, length: UInt8) async throws -> Data {
        let result = try await iso7816Tag.apduCommand(data: Data([0x00, 0xB0, 0x80 | efId, offset, length])) { result in
            throw MyNumberCardReadError.unknownError
        }
        return result.0
    }
    
    func readBinary(_ iso7816Tag: NFCISO7816Tag, efId: UInt8, offset: UInt8, length: (UInt8, UInt8)) async throws -> Data {
        let result = try await iso7816Tag.apduCommand(data: Data([0x00, 0xB0, 0x80 | efId, offset, 0x00, length.0, length.1])) { result in
            throw MyNumberCardReadError.unknownError
        }
        return result.0
    }
    
    func generateDigestInfo(for data: Data) -> Data? {
        // SHA-256 の OID: 2.16.840.1.101.3.4.2.1
        let sha256OID: [UInt8] = [0x30, 0x31, 0x30, 0x0d, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x01, 0x05, 0x00, 0x04, 0x20]

        // メッセージの SHA-256 ハッシュを計算
        let hash = SHA256.hash(data: data)
        var digestInfo = Data(sha256OID)
        digestInfo.append(contentsOf: hash)
        
        return digestInfo
    }

    func computeDigitalSignature(_ iso7816Tag: NFCISO7816Tag, hashAlgorithm: HashAlgorithm, hash: Data) async throws -> Data {
        print("sha256", hash.map { String(format: "%02hhX", $0)}.joined(separator: " "));
        guard hash.count == 0x20 else {
            throw MyNumberCardReadError.invalidHashLength
        }
        let commandData = Data([0x80, 0x2A, 0x00, 0x80, 0x33,
                                0x30, 0x31, 0x30, 0x0d, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02,
                                0x01, 0x05,
                                0x00, 0x04, 0x20]) + hash + Data([0x00])
        let result = try await iso7816Tag.apduCommand(data: commandData) { result in
            throw MyNumberCardReadError.unknownError
        }
        print("signature", result.0.map { String(format: "%02hhX", $0)}.joined(separator: " "));
        return result.0
    }

    func computeDigitalSignatureRaw(_ iso7816Tag: NFCISO7816Tag, raw: Data) async throws -> Data {
        guard raw.count < 0x100 else {
            throw MyNumberCardReadError.invalidSignatureTargetLength
        }
        let commandData = Data([0x80, 0x2A, 0x00, 0x80, UInt8(raw.count)]) + raw + Data([0x00])
        let result = try await iso7816Tag.apduCommand(data: commandData) { result in
            throw MyNumberCardReadError.unknownError
        }
        print("signature", result.0.map { String(format: "%02hhX", $0)}.joined(separator: " "));
        return result.0
    }

    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        print("active")
        session.alertMessage = "マイナンバーカードをかざしてください..."
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        print("invalidate")
        if continuation != nil {
            print("continuation")
            continuation?.resume(throwing: MyNumberCardReadError.unknownError)
            continuation = nil
        } else if invalidationContinuation != nil {
            print("invalidation continuation: \(error)")
            invalidationContinuation?.resume()
            invalidationContinuation = nil
        }
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        print("detect")
        continuation?.resume(returning: tags.first!)
        continuation = nil
    }

    func readerSession(_ session: NFCTagReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
    }
}

extension NFCISO7816Tag {
    func apduCommand(data: Data, onError: ((Data, UInt8, UInt8)) throws -> Void) async throws -> (Data, UInt8, UInt8) {
        let response = try await self.sendCommand(apdu: NFCISO7816APDU(data: data)!)
        if (!isAPDUCommandSuccess(response: response)) {
            try onError(response)
        }
        return response
    }
}

func isAPDUCommandSuccess(response: (Data, UInt8, UInt8)) -> Bool {
    return response.1 == 0x90 && response.2 == 0x00;
}

enum MyNumberCardAP {
    case CardInfoInputHelper
    case JPKI
}

enum JPKICertType {
    case PublicAuthCertificate
    case DigitalSignatureCertificate
}

enum MyNumberCardReadError: Error {
    case notImplemented(ap: MyNumberCardAP)
    case pinChallengeFailed(remainingNumberOfAttempts: UInt8)
    case invalidHashLength
    case invalidSignatureTargetLength
    case unknownError
}

enum HashAlgorithm {
    case SHA256
}

extension Data {
    var sha256: Data {
        let digest = SHA256.hash(data: self)
        return Data(digest)
    }
}
