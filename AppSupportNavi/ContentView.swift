//
//  ContentView.swift
//  AppSupportNavi
//
//  Created by chephes on 1/26/23.
//

import SwiftUI
import WebKit

enum ActiveSheet: Identifiable {
    case mynumberCardTester, web
    
    var id: Int {
        hashValue
    }
}

struct TextInputDialog: View {
    @Binding var isPresented: Bool
    @Binding var text: String
    var title: String
    var message: String
    var placeholder: String
    var action: () -> Void

    var body: some View {
        VStack {
            Text(title)
                .font(.headline)
            Text(message)
                .font(.subheadline)
                .padding(.bottom)

            TextField(placeholder, text: $text)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.horizontal)

            HStack {
                Button("キャンセル") {
                    isPresented = false
                }
                Spacer()
                Button("OK") {
                    action()
                    isPresented = false
                }
            }
            .padding(.top)
        }
        .padding()
        .background(Color(.systemBackground))
        .cornerRadius(10)
        .shadow(radius: 10)
    }
}

struct MynumberCardJPKITesterView: View {
    @State private var showDialog = false;
    @State private var pinNumber = "";
    @State private var result = "";
    var viewModel = MyNumberCardReaderModel()

    var body: some View {
        VStack {
            ScrollView {
                Text(result)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                    .font(.system(size: 11, design: .monospaced))
                    .foregroundColor(.black)
            }
            Button("トークン読み取り") {
                Task.init {
                    result = try await viewModel.readToken()
                }
            }
            Button("利用者証明書取得") {
//                Task.init {
//                    let data = try await viewModel.readUserCertificate()
//                    result = data.map { String(format: "%02hhX", $0)}.joined(separator: " ")
//                }
            }
            Button("電子署名付与") {
                Task.init {
//                    let data = try await self.viewModel.requestComputeDigitalSignature("ABC123")
//                    result = data.map { String(format: "%02hhX", $0)}.joined(separator: " ")
//                    print(data.base64EncodedString());
                }
//                showDialog = true
////                Task.init {
//                    let data = try await viewModel.computeDigitalSignature()
//                }
            }
        }
        .background(Color(.white))
//        .sheet(isPresented: $showDialog) {
//            ZStack {
//                Color.black.opacity(0.4).ignoresSafeArea()
//                TextInputDialog(isPresented: $showDialog, text: $pinNumber, title: "PIN入力", message: "PIN(6桁)を入力してください", placeholder: "******") {
//                }
//            }
//        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct MynumberCardTesterView: View {
    @State private var showDialog = false;
    @State private var pinNumber = "";
    @StateObject var viewModel = NFCViewModel()

    var body: some View {
        VStack {
            ScrollView {
                Text(viewModel.log)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                    .font(.system(size: 11, design: .monospaced))
                    .foregroundColor(.black)
            }
            HStack {
                Button("読み取る") {
                    showDialog = true
                }
                Button("ログをコピー") {
                    UIPasteboard.general.string = viewModel.log
                }
            }
        }
        .background(Color(.white))
        .sheet(isPresented: $showDialog) {
            ZStack {
                Color.black.opacity(0.4).ignoresSafeArea()
                TextInputDialog(isPresented: $showDialog, text: $pinNumber, title: "PIN入力", message: "PIN(4桁)を入力してください", placeholder: "****") {
                    self.viewModel.startScan(pinNumber)
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct ContentView: View {
//    static let url = "https://ed75-60-237-244-28.ngrok-free.app/app_index.html";
    static let url = "https://demo-city.form.kosodate-town.com/app_index.html";
//    let url = "https://app-dev.imabari.navitest.kosodate-town.com/app_top.hml"
    var webviewConfiguration: WKWebViewConfiguration = {
        let config = WKWebViewConfiguration()
        config.processPool = WKProcessPool()
        return config
    }()

    var body: some View {
        WebViewWithToolbar(state: WebViewStateModel(home: URL(string: ContentView.url)!, initialSessionStorage: nil, shouldShowToolbar: false), configuration: webviewConfiguration, closeAction: nil)
            .preferredColorScheme(.light)
            .edgesIgnoringSafeArea(.bottom)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
