//
//  NFCViewModel.swift
//  AppSupportNavi
//
//  Created by chephes on 4/27/23.
//

import Foundation
import CoreNFC

class NFCViewModel: NSObject, NFCTagReaderSessionDelegate, ObservableObject {
    var tag: NFCISO7816Tag?
    var session: NFCTagReaderSession?
    var pinNumber: String?
    @Published var log: String = "マイナンバーカード読み取りテスト"

    func startScan(_ pinNumber: String) {
        self.pinNumber = pinNumber
        self.session = NFCTagReaderSession(pollingOption: [.iso14443], delegate: self)
        self.session?.begin()
    }
    
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        let tag = tags.first!
        switch tag {
        case let .iso7816(tag):
            print(tag)
            Task.init {
                do {
                    try await session.connect(to: tags.first!)
                    await self.connectISO7816Tag(tag)
                    session.invalidate()
                } catch {
                    
                }
            }
        default:
            break
        }
    }
    
    func apduCommand(_ iso7816Tag: NFCISO7816Tag, data: Data) async throws -> (Data, UInt8, UInt8) {
        let response = try await iso7816Tag.sendCommand(apdu: NFCISO7816APDU(data: data)!)
        DispatchQueue.main.async {
            let received = response.0.map { String(format: "%02hhX", $0)}.joined(separator: " ")
            print(received)
            self.log = """
-> \(received) \(String(format:"%02X %02X", response.1, response.2))
<- \(data.map { String(format: "%02hhX", $0)}.joined(separator: " "))
-------------------
\(self.log)
"""
        }
        return response
    }
    
    func apduCommand_(_ iso7816Tag: NFCISO7816Tag, data: Data) async throws {
        let _ = try await apduCommand(iso7816Tag, data: data)
    }
    
    func verifyNumberOfRemainingAttempts(_ iso7816Tag: NFCISO7816Tag) async throws {
        try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x04, 0x00, 0x06, 0xA0, 0x00, 0x00, 0x02, 0x31, 0x01, 0x00]));
        try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x04, 0x00, 0x06, 0xA0, 0x00, 0x00, 0x02, 0x31, 0x01]));
        try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x00, 0x00]));
        try await apduCommand_(iso7816Tag, data: Data([0x00, 0x20, 0x00, 0x81]));
        try await apduCommand_(iso7816Tag, data: Data([0x00, 0x20, 0x00, 0x82]));
    }
    
    func connectISO7816Tag(_ iso7816Tag: NFCISO7816Tag) async {
        session!.alertMessage = "マイナンバーカードを読み取り中...";
//        let apduCommand = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0xA4, p1Parameter: 0x04, p2Parameter: 0x00, data: Data([0xA0, 0x00, 0x00, 0x02, 0x31, 0x01]), expectedResponseLength: 0)
        do {
            try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x04, 0x0C, 0x0A, 0xD3, 0x92, 0x10, 0x00, 0x31, 0x00, 0x01, 0x01, 0x04, 0x08]));
            // PIN
            try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x02, 0x0C, 0x02, 0x00, 0x11]));
            let asciiArray: [UInt8] = pinNumber!.utf8.map { $0 }
            // Challenge
            try await apduCommand_(iso7816Tag, data: Data([0x00, 0x20, 0x00, 0x80, 0x04, asciiArray[0], asciiArray[1], asciiArray[2], asciiArray[3]]));
            for i in 1...10 {
                try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x02, 0x0C, 0x02, 0x00, UInt8(i)]));
                try await apduCommand_(iso7816Tag, data: Data([0x00, 0xB0, 0x00, 0x00, 0x00]));
            }
//            try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x02, 0x0C, 0x02, 0x2F, 0x01])); // Select EF 0x2F01
//            try await apduCommand_(iso7816Tag, data: Data([0x00, 0xB0, 0x00, 0x00, 0x11])); // EF 0x2F01を全て(17バイト)読み出す
//            try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x02, 0x0C, 0x02, 0x00, 0x0A])); // Select EF 0x0A
//            try await apduCommand_(iso7816Tag, data: Data([0x00, 0xB0, 0x00, 0x00, 0x03])); // EF 0x000Aを全て(3バイト)読み出す
//            try await apduCommand_(iso7816Tag, data: Data([0x00, 0x20, 0x00, 0x81, 0x04, 0x31, 0x37, 0x32, 0x39])); // pin1を照合
//            try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x04, 0x0C, 0x06, 0xA0, 0x00, 0x00, 0x02, 0x31, 0x01])); // Select DF1
//            try await apduCommand_(iso7816Tag, data: Data([0x00, 0xA4, 0x02, 0x0C, 0x02, 0x00, 0x01])); // Select EF 0x01 (EF01)
//            let _ = try await apduCommand(iso7816Tag, data: Data([0x00, 0xB0, 0x00, 0x00, 0x00, 0x03, 0x70])); // EF 0x0001を880バイト読み出す
         } catch let error {
             self.log = """
 error: \(error)
 -------------------
 \(self.log)
 """
        }
    }

    func readerSession(_ session: NFCTagReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
    }
}

enum TestError: Error {
    case sampleError
}
