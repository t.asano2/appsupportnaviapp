//
//  WebView.swift
//  AppSupportNavi
//
//  Created by chephes on 4/24/23.
//

import SwiftUI
import WebKit
import Combine

class WebViewStateModel: ObservableObject, Identifiable {
    @Published var isLoading = false
    @Published var canGoBack = false
    @Published var canGoForward = false
    @Published var shouldGoBack = false
    @Published var shouldGoForward = false
    @Published var shouldLoad = false
    @Published var title = ""
    @Published var modalStateModel: WebViewStateModel?
    @Published var closeButtonLabel: CloseButtonType = .text("閉じる")
    @Published var isKeyboardVisible = false
    var cancellables = Set<AnyCancellable>()
    
    struct Error {
        let code: URLError.Code
        let message: String
    }
    @Published var error: Error?
    
    private(set) var url: URL
    private(set) var initialSessionStorage: String?
    let home: URL
    let shouldShowToolbar: Bool
    
    init(home: URL, initialSessionStorage: String?, shouldShowToolbar: Bool) {
        self.url = home
        self.home = home
        self.initialSessionStorage = initialSessionStorage
        self.shouldShowToolbar = shouldShowToolbar
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)
            .sink { _ in
                withAnimation {
                    self.isKeyboardVisible = true
                }
            }
            .store(in: &cancellables)
        
        // キーボード非表示時
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)
            .sink { _ in
                withAnimation {
                    self.isKeyboardVisible = false
                }
            }
            .store(in: &cancellables)
    }

    func load(_ url: URL) {
        self.url = url
        shouldLoad = true
    }
}

struct WebViewWithToolbar: View {
    @ObservedObject var state: WebViewStateModel
    @State private var progress: CGFloat = 0.0
    @State private var showProgressBar: Bool = true
    var configuration: WKWebViewConfiguration
    
    var closeAction: (() -> Void)?

    var body: some View {
        ZStack {
            Color.white.edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 0) {
                WebView(stateModel: state, configuration: configuration, progress: $progress)
                    .onChange(of: progress) { newValue in
                        if newValue == 1.0 {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                withAnimation {
                                    showProgressBar = false
                                }
                            }
                        } else {
                            withAnimation {
                                showProgressBar = true
                            }
                        }
                    }
                if showProgressBar {
                    ProgressBar(progress: $progress)
                }
                if state.shouldShowToolbar && !state.isKeyboardVisible {
                    Toolbar(webViewState: state, closeAction: closeAction)
                }
            }
            
            if state.isLoading {
                Color.black.opacity(0.4)
                    .edgesIgnoringSafeArea(.all)

                // システムのローディングインジケータ
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
                    .scaleEffect(1.5, anchor: .center)
            }
        }
        .fullScreenCover(item: $state.modalStateModel) { modalState in
            WebViewWithToolbar(state: modalState, configuration: configuration) {
                self.state.modalStateModel = nil
            }
        }
    }
}

struct ProgressBar: View {
    @Binding var progress: CGFloat // 0.0から1.0まで
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle()
                    .opacity(0.3)
                    .frame(width: geometry.size.width, height: 4.0)
                
                Rectangle()
                    .frame(width: geometry.size.width * self.progress, height: 4.0)
                    .foregroundColor(.blue)
            }
        }
        .frame(height: 4)
    }
}

enum CloseButtonType {
    case text(String)
    case image(String)
}

struct Toolbar: View {
    @ObservedObject var webViewState: WebViewStateModel
    var closeAction: (() -> Void)?

    var body: some View {
        HStack {
            Button(action: {
                webViewState.shouldGoBack = true
            }) {
                Image(webViewState.canGoBack ? "arrow_1" : "arrow_2")
                    .resizable()
                    .frame(width: 32, height: 32)
            }
            .disabled(!webViewState.canGoBack)
            Button(action: {
                webViewState.shouldGoForward = true
            }) {
                Image(webViewState.canGoForward ? "arrow_1" : "arrow_2")
                    .resizable()
                    .scaleEffect(x: -1, y: 1)
                    .frame(width: 32, height: 32)
            }
            .disabled(!webViewState.canGoForward)

            Spacer()
            
            Button(action: {
                closeAction?()
            }) {
                switch webViewState.closeButtonLabel {
                case .text(let text):
                    Text(text)
                case .image(let imageName):
                    Image(imageName)
                        .resizable()
                        .frame(width: 32, height: 32)
                }
            }
        }
        .padding()
        .background(Color(red: 0.97, green: 0.97, blue: 0.97))
    }
}

struct WebView: UIViewRepresentable {
    @ObservedObject var stateModel: WebViewStateModel
    var viewModel = MyNumberCardReaderModel()
    var contentController = WKUserContentController()
    var configuration: WKWebViewConfiguration
    @Binding var progress: CGFloat

    func makeCoordinator() -> Coordinator {
        let coordinator = Coordinator(self, stateModel: stateModel)
        return coordinator
    }

    func makeUIView(context: Context) -> WKWebView {
        configuration.userContentController = contentController
        let webView = WKWebView(frame: .zero, configuration: configuration)
        let consoleLogOverrideScript = """
var originalConsoleLog = console.log;
console.log = (...args) => {
    window.webkit.messageHandlers.consoleLog.postMessage(args);
};
1;
"""
        if let initialSessionStorage = stateModel.initialSessionStorage {
            print("storage: \(initialSessionStorage)")
            let deserializeScript = """
                var storage = \(initialSessionStorage);
                for (var key in storage) {
                    sessionStorage.setItem(key, storage[key]);
                }
            """
            let sessionStorageScript = WKUserScript(source: deserializeScript, injectionTime: .atDocumentStart, forMainFrameOnly: true)
            contentController.addUserScript(sessionStorageScript)
        }

        let userScript = WKUserScript(source: consoleLogOverrideScript, injectionTime: .atDocumentStart, forMainFrameOnly: false)
        contentController.addUserScript(userScript)
        contentController.add(context.coordinator, name: "scanMyNumberCardForBasic4Info")
        contentController.add(context.coordinator, name: "consoleLog")
        contentController.add(context.coordinator, name: "showPinInputDialog")
        contentController.add(context.coordinator, name: "alert")
        contentController.add(context.coordinator, name: "readPublicAuthCertificate")
        contentController.add(context.coordinator, name: "signWithPublicAuthKey")
        contentController.add(context.coordinator, name: "readDigitalSignatureCertificate");
        contentController.add(context.coordinator, name: "signWithDigitalSignatureKey");
        contentController.add(context.coordinator, name: "signWithPublicAuthKeyRaw")
        contentController.add(context.coordinator, name: "signWithDigitalSignatureKeyRaw");

        webView.navigationDelegate = context.coordinator
        webView.uiDelegate = context.coordinator
        context.coordinator.observeProgress(of: webView)
        webView.evaluateJavaScript("navigator.userAgent") { result, error in
            if let userAgent = result as? String {
                webView.customUserAgent = "\(userAgent) Version/16.4 Safari/604.1 AsukoeGDXApp/1.0"
                print(userAgent)
            } else {
                print("Failed to get the UserAgent: \(error?.localizedDescription ?? "Unknown error")")
            }
        }
        
        webView.load(URLRequest(url: stateModel.url))
        return webView
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {
        if stateModel.shouldGoBack {
            uiView.goBack()
            stateModel.shouldGoBack = false
        }
        if stateModel.shouldGoForward {
            uiView.goForward()
            stateModel.shouldGoForward = false
        }
        if stateModel.shouldLoad {
            let request = URLRequest(url: stateModel.url)
            uiView.load(request)
            stateModel.shouldLoad = false
        }
    }

    class Coordinator: NSObject, WKNavigationDelegate, WKScriptMessageHandler, WKUIDelegate {
        var parent: WebView
        var observation: NSKeyValueObservation?
        
        @ObservedObject private var stateModel: WebViewStateModel

        init(_ parent: WebView, stateModel: WebViewStateModel) {
            self.parent = parent
            self.stateModel = stateModel
        }
        
        func observeProgress(of webView: WKWebView) {
            self.observation = webView.observe(\.estimatedProgress, options: [.new]) { _, change in
                if let newProgress = change.newValue {
                    DispatchQueue.main.async {
                        self.parent.progress = CGFloat(newProgress)
                    }
                }
            }
        }
        
        deinit {
            observation?.invalidate()
        }
        
        func alertDialog(webView: WKWebView, title: String, message: String, callback: @escaping () -> Void) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            // OKアクション
            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                Task.init {
                    callback()
                }
            }
            
            alertController.addAction(okAction)
            
            // ダイアログを表示
            if let viewController = webView.window?.rootViewController {
                var topViewController = viewController
                while let presentedViewController = topViewController.presentedViewController {
                    topViewController = presentedViewController
                }
                topViewController.present(alertController, animated: true, completion: nil)
            }
        }
        
        func showPinInputDialog(webView: WKWebView, title: String, message: String, callback: @escaping (String?) -> Void) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            // UITextField追加
            alertController.addTextField { textField in
                textField.placeholder = "PINを入力"
                textField.keyboardType = .default
                textField.isSecureTextEntry = true // パスワード入力のように隠す場合
            }
            
            // キャンセルアクション
            let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel) { _ in
                callback(nil)
            }
            
            // OKアクション
            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                if let pin = alertController.textFields?.first?.text {
                    if pin.range(of: "^[a-zA-Z0-9]{4,8}$", options: .regularExpression) != nil {
                        callback(pin)
                    } else {
                        self.alertDialog(webView: webView, title: "PINエラー", message: "PIN入力に誤りがあります") {
                            callback(nil)
                        }
                    }
                } else {
                    callback(nil)
                }
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            
            // ダイアログを表示
            if let viewController = webView.window?.rootViewController {
                var topViewController = viewController
                while let presentedViewController = topViewController.presentedViewController {
                    topViewController = presentedViewController
                }
                topViewController.present(alertController, animated: true, completion: nil)
            }
        }
        
        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
            let name = message.name
            guard let webView = message.webView else {
                print("webview is missing")
                return
            }
            print(name)
            if name == "scanMyNumberCardForBasic4Info" {
                showPinInputDialog(webView: webView, title: "PIN入力", message: "マイナンバーカードのPINを入力してください") { pin in
                    guard let pin else {
                        webView.appCallbackWithError(name: name, error: "{\"kind\":\"pinInputCanceled\",\"description\":\"pin input canceled\"}")
                        return
                    }
                    Task.init {
                        do {
                            let basic4Info = try await self.parent.viewModel.readBasic4Info(pin)
                            let encoder = JSONEncoder()
                            encoder.outputFormatting = .prettyPrinted
                            let jsonData = try encoder.encode(basic4Info)
                            if let jsonString = String(data: jsonData, encoding: .utf8) {
                                webView.appCallback(name: name, jsonString: jsonString)
                            }
                        } catch MyNumberCardReadError.pinChallengeFailed(let remainingNumberOfAttempts) {
                            self.alertDialog(webView: webView, title: "読み取り失敗", message: "PIN が間違っています。\nあと\(remainingNumberOfAttempts)回間違えるとカードがロックされます。") {
                                webView.appCallbackWithError(name: name, error: "{\"kind\":\"pinChallengeFailed\",\"remainingNumberOfAttempts\":\(remainingNumberOfAttempts)}")
                            }
                        } catch {
                            webView.appCallbackWithError(name: name, error: "{\"kind\":\"unknown\",\"description\":\"\(error.localizedDescription)\"}")
                        }
                    }
                }
            } else if name == "consoleLog" {
                print(message.body)
            } else if name == "showPinInputDialog" {
            } else if name == "alert" {
                guard let body = message.body as? [String: String] else {
                    webView.appCallbackWithError(name: name, error: "{\"kind\":\"invalid_argument\",\"description\":\"body is missing\"}")
                    return
                }
                alertDialog(webView: webView, title: body["title"] ?? "アラート", message: body["message"] ?? "") {
                    webView.appCallback(name: name, jsonString: "null")
                }
            } else if name == "readPublicAuthCertificate" {
                Task.init {
                    do {
                        let publicAuthCertificate = try await parent.viewModel.readPublicAuthCertificate()
                        let jsonString = "\"\(publicAuthCertificate.base64EncodedString())\""
                        webView.appCallback(name: name, jsonString: jsonString)
                    } catch {
                        webView.appCallbackWithError(name: name, error: "{\"kind\":\"unknown\",\"description\":\"\(error.localizedDescription)\"}")
                    }
                }
            } else if name == "signWithPublicAuthKey" {
                guard let body = message.body as? [String: Any] else {
                    webView.appCallbackWithError(name: name, error: "{\"kind\":\"invalid_argument\",\"description\":\"body is missing\"}")
                    return
                }
                guard let base64Hash = body["hash"] as? String else {
                    webView.appCallbackWithError(name: name, error: "{\"kind\":\"invalid_argument\",\"description\":\"hash is missing\"}")
                    return
                }
                guard "sha256" == body["algorithm"] as? String else {
                    webView.appCallbackWithError(name: name, error: "{\"kind\":\"invalid_argument\",\"description\":\"unsuported hash algorithm \(String(describing: body["algorithm"] as? String))\"}")
                    return
                }
                showPinInputDialog(webView: webView, title: "PIN入力", message: "利用者証明用PIN(数字4桁)を入力してください") { pin in
                    guard let pin else {
                        webView.appCallbackWithError(name: name, error: "{\"kind\":\"pin_input_canceled\",\"description\":\"pin input canceled\"}")
                        return
                    }
                    Task.init {
                        do {
                            let result = try await self.parent.viewModel.signWithPublicAuthKey(pin, hashAlgorithm: .SHA256, base64Hash: base64Hash)
                            let base64Cert = result.0.base64EncodedString()
                            let base64Signature = result.1.base64EncodedString()
                            let jsonString = "{\"cert\": \"\(base64Cert)\",\"signature\":\"\(base64Signature)\" }"
                            webView.appCallback(name: name, jsonString: jsonString)
                        } catch MyNumberCardReadError.pinChallengeFailed(let remainingNumberOfAttempts) {
                            self.alertDialog(webView: webView, title: "読み取り失敗", message: "PIN が間違っています。\nあと\(remainingNumberOfAttempts)回間違えるとカードがロックされます。") {
                                webView.appCallbackWithError(name: name, error: "{\"kind\":\"pinChallengeFailed\",\"remainingNumberOfAttempts\":\(remainingNumberOfAttempts)}")
                            }
                        } catch {
                            webView.appCallbackWithError(name: name, error: "{\"kind\":\"unknown\",\"description\":\"\(error.localizedDescription)\"}")
                        }
                    }
                }
            } else if name == "readDigitalSignatureCertificate" {
                showPinInputDialog(webView: webView, title: "PIN入力", message: "電子署名用PIN(英数字6桁)を入力してください") { pin in
                    guard let pin else {
                        webView.appCallbackWithError(name: name, error: "{\"kind\":\"pin_input_canceled\",\"description\":\"pin input canceled\"}")
                        return
                    }
                    Task.init {
                        do {
                            let signatureCertificate = try await self.parent.viewModel.readDigitalSignatureCertificate(pin)
                            let jsonString = "\"\(signatureCertificate.base64EncodedString())\""
                            webView.appCallback(name: name, jsonString: jsonString)
                        } catch {
                            webView.appCallbackWithError(name: name, error: "\"\(error.localizedDescription)\"")
                        }
                    }
                }
            } else if name == "signWithDigitalSignatureKey" {
                guard let body = message.body as? [String: Any] else {
                    print("Failed to get the message content")
                    return
                }
                guard let data = body["hash"] as? String else {
                    print("Failed to get the message content")
                    return
                }
                guard "sha256" == body["algorithm"] as? String else {
                    webView.appCallbackWithError(name: name, error: "{\"kind\":\"invalid_hash_algorithm\",\"description\":\"unsuported hash algorithm \(String(describing: body["algorithm"] as? String))\"}")
                    return
                }
                showPinInputDialog(webView: webView, title: "PIN入力", message: "電子署名用PIN(英数字6桁)を入力してください") { pin in
                    guard let pin else {
                        webView.appCallbackWithError(name: name, error: "{\"kind\":\"pin_input_canceled\",\"description\":\"pin input canceled\"}")
                        return
                    }
                    Task.init {
                        do {
                            let result = try await self.parent.viewModel.signWithDigitalSignatureKey(pin, hashAlgorithm: .SHA256, base64Hash: data)
                            let base64Cert = result.0.base64EncodedString()
                            let base64Signature = result.1.base64EncodedString()
                            let jsonString = "{\"cert\": \"\(base64Cert)\",\"signature\":\"\(base64Signature)\" }"
                            webView.appCallback(name: name, jsonString: jsonString)
                        } catch {
                            webView.appCallbackWithError(name: name, error: "\"\(error.localizedDescription)\"")
                        }
                    }
                }
            } else if name == "signWithPublicAuthKeyRaw" {
                guard let body = message.body as? String else {
                    webView.appCallbackWithError(name: name, error: "{\"kind\":\"invalid_argument\",\"description\":\"body is missing\"}")
                    return
                }
                showPinInputDialog(webView: webView, title: "PIN入力", message: "利用者証明用PIN(数字4桁)を入力してください") { pin in
                    guard let pin else {
                        webView.appCallbackWithError(name: name, error: "{\"kind\":\"pin_input_canceled\",\"description\":\"pin input canceled\"}")
                        return
                    }
                    Task.init {
                        do {
                            let result = try await self.parent.viewModel.signWithPublicAuthKeyRaw(pin, base64Data: body)
                            let base64Cert = result.0.base64EncodedString()
                            let base64Signature = result.1.base64EncodedString()
                            let jsonString = "{\"cert\": \"\(base64Cert)\",\"signature\":\"\(base64Signature)\" }"
                            webView.appCallback(name: name, jsonString: jsonString)
                        } catch {
                            webView.appCallbackWithError(name: name, error: "{\"kind\":\"unknown\",\"description\":\"\(error.localizedDescription)\"}")
                        }
                    }
                }
            } else if name == "signWithDigitalSignatureKeyRaw" {
                guard let body = message.body as? String else {
                    print("Failed to get the message content")
                    return
                }
                showPinInputDialog(webView: webView, title: "PIN入力", message: "電子署名用PIN(英数字6桁)を入力してください") { pin in
                    guard let pin else {
                        webView.appCallbackWithError(name: name, error: "{\"kind\":\"pin_input_canceled\",\"description\":\"pin input canceled\"}")
                        return
                    }
                    Task.init {
                        do {
                            let result = try await self.parent.viewModel.signWithDigitalSignatureKeyRaw(pin, base64Data: body)
                            let base64Cert = result.0.base64EncodedString()
                            let base64Signature = result.1.base64EncodedString()
                            let jsonString = "{\"cert\": \"\(base64Cert)\",\"signature\":\"\(base64Signature)\" }"
                            webView.appCallback(name: name, jsonString: jsonString)
                        } catch {
                            webView.appCallbackWithError(name: name, error: "\"\(error.localizedDescription)\"")
                        }
                    }
                }
            } else {
                print(message.name)
            }
        }
        
        func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
            print("Error loading page: \(error.localizedDescription)")
        }
        
        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
            print("Error loading page: \(error.localizedDescription)")
        }
        
        func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic {
                let user = "admin"
                let password = "typextest"
                let credential = URLCredential(user: user, password: password, persistence: .forSession)
                completionHandler(.useCredential, credential)
            } else {
                completionHandler(.performDefaultHandling, nil)
            }
        }
        
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            parent.stateModel.canGoBack = webView.canGoBack
            parent.stateModel.canGoForward = webView.canGoForward
//            parent.stateModel.isLoading = false
        }
        
        func openInNewWindow(url: URL, sessionStorage: String? = nil) {
            let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
            let modalStateModel = WebViewStateModel(home: url, initialSessionStorage: sessionStorage, shouldShowToolbar: true)
            if let closeButtonLabelItem = components?.queryItems?.first(where: { $0.name == "__gdxappclosebutton__" }) {
                if let closeButtonLabelItem = closeButtonLabelItem.value {
                    if closeButtonLabelItem == "image.home" {
                        modalStateModel.closeButtonLabel = .image("home_1")
                    } else {
                        modalStateModel.closeButtonLabel = .text(closeButtonLabelItem)
                    }
                } else {
                    modalStateModel.closeButtonLabel = .text("閉じる")
                }
            }
            parent.stateModel.modalStateModel = modalStateModel
        }
        
        func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration,
                     for navigationAction: WKNavigationAction,
                     windowFeatures: WKWindowFeatures) -> WKWebView? {
            
            if let url = navigationAction.request.url {
                let serializeScript = """
                    var storage = {};
                    for (var i = 0; i < sessionStorage.length; i++) {
                        var key = sessionStorage.key(i);
                        var value = sessionStorage.getItem(key);
                        storage[key] = value;
                    }
                    JSON.stringify(storage);
                """
                webView.evaluateJavaScript(serializeScript) { result, error in
                    if let error = error {
                        print("Error: \(error)")
                    }
                    self.openInNewWindow(url: url, sessionStorage: result as! String?)
                }
            }

            return nil
        }

        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            if navigationAction.targetFrame == nil {
                guard let url = navigationAction.request.url else {
                    decisionHandler(.allow)
                    return
                }
                openInNewWindow(url: url, sessionStorage: nil)
                decisionHandler(.cancel)
                return
            }
            guard navigationAction.targetFrame?.isMainFrame == true else {
                decisionHandler(.allow)
                return
            }
            if let url = navigationAction.request.url {
                if url == parent.stateModel.home {
//                    parent.stateModel.isLoading = true
                    decisionHandler(.allow)
                    return
                }
                let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
                if let item = components?.queryItems?.first(where: { $0.name == "__gdxappmodal__" && $0.value == "true" }) {
                    var modifiedComponents = components
                    modifiedComponents?.queryItems?.removeAll { $0 == item }
                    let modifiedURL = modifiedComponents?.url ?? url
                    openInNewWindow(url: modifiedURL)
                    decisionHandler(.cancel)
                    return
                }
            }
//            parent.stateModel.isLoading = true
            decisionHandler(.allow)
        }
    }
}

//enum MyNumberCardReadError: Error {
//    case notImplemented(ap: MyNumberCardAP)
//    case pinChallengeFailed(remainingNumberOfAttempts: UInt8)
//    case unknownError
//}

extension WKWebView {
    func appCallback(name: String, jsonString: String?) {
        let jsJsonString = jsonString != nil ? jsonString! : "null"
        let jsCommand = "window._appCallback(\"\(name)\", \(jsJsonString), null)"
        self.evaluateJavaScript(jsCommand, completionHandler: nil)
    }
    
    // error専用のメソッド
    func appCallbackWithError(name: String, error: String) {
        let jsCommand = "window._appCallback(\"\(name)\", null, \(error))"
        self.evaluateJavaScript(jsCommand, completionHandler: nil)
    }
}
