//
//  JPDriversLicenseRecordData.swift
//  AppSupportNavi
//
//  Created by chephes on 2/8/23.
//

import Foundation

enum JPDriversLicenseRecordDataReadError: Error {
    case TagDoesNotMatch(tag: UInt8)
    case InvalidLength
}

class JPDriversLicenseRecordDataReader {
    var data: Data;
    var offset: Int = 0
    
    init(data: Data, offset: Int) {
        self.data = data
        self.offset = offset
    }
    
    func readHex(tag: UInt8) throws -> UInt8 {
        let type = data[offset]
        if type != tag {
            throw JPDriversLicenseRecordDataReadError.TagDoesNotMatch(tag: tag)
        }
        offset += 1
        let length = data[offset]
        if length != 1 {
            throw JPDriversLicenseRecordDataReadError.InvalidLength
        }
        offset += 1
        let value = data[offset]
        offset += 1
        return value
    }

    func read0208(tag: UInt8) throws -> String {
        let type = data[offset]
        if type != tag {
            throw JPDriversLicenseRecordDataReadError.TagDoesNotMatch(tag: tag)
        }
        offset += 1
        let length = data[offset]
        offset += 1
        let range = offset ..< offset + Int(length)
        offset += Int(length)
        var subdata = data.subdata(in: range)
        let jisMarkup = Data([0x1B, 0x24, 0x42])
        subdata.insert(contentsOf: jisMarkup, at: 0)
        return String(data: subdata, encoding: .iso2022JP)!
    }

    func read0201(tag: UInt8) throws -> String {
        let type = data[offset]
        if type != tag {
            throw JPDriversLicenseRecordDataReadError.TagDoesNotMatch(tag: tag)
        }
        offset += 1
        let length = data[offset]
        offset += 1
        let range = offset ..< offset + Int(length)
        offset += Int(length)
        return String(data: data.subdata(in: range), encoding: .iso2022JP)!
    }
}

struct JPDriversLicenseRecordData : Equatable {
    let jisYearNumber: UInt8
    let name: String
    let nameReading: String
    let nickname: String
    let uniformNameReading: String
    let birthdate: String
    let address: String
    let deliveryDate: String
    let inquiryNumber: String
    let licenseColorClassification: String
    let validUntil: String
    let condition1: String
    let condition2: String
    let condition3: String
    let condition4: String
    let publicSecurityCommitteeName: String
    let licenseNumber: String
    let licenseDate2andSmallandMotorbike: String
    let licenseDateOthers: String
    let licenseDateType2: String
    let licenseDateLarge: String
    let licenseDateNormal: String
    let licenseDateLargeSpecial: String
    let licenseDateLargeBike: String
    let licenseDateNormalBike: String
    let licenseDateSmallSpecial: String
    let licenseDateMotorbike: String
    let licenseDateTowing: String
    let licenseDateLarge2: String
    let licenseDateNormal2: String
    let licenseDateLargeSpecial2: String
    let licenseDateTowing2: String
    let licenseDateMiddle: String
    let licenseDateMiddle2: String
    let licenseDateSemiMiddle: String
    
    init(jisYearNumber: UInt8, name: String, nameReading: String, nickname: String, uniformNameReading: String, birthdate: String, address: String, deliveryDate: String, inquiryNumber: String, licenseColorClassification: String, validUntil: String, condition1: String, condition2: String, condition3: String, condition4: String, publicSecurityCommitteeName: String, licenseNumber: String, licenseDate2andSmallandMotorbike: String, licenseDateOthers: String, licenseDateType2: String, licenseDateLarge: String, licenseDateNormal: String, licenseDateLargeSpecial: String, licenseDateLargeBike: String, licenseDateNormalBike: String, licenseDateSmallSpecial: String, licenseDateMotorbike: String, licenseDateTowing: String, licenseDateLarge2: String, licenseDateNormal2: String, licenseDateLargeSpecial2: String, licenseDateTowing2: String, licenseDateMiddle: String, licenseDateMiddle2: String, licenseDateSemiMiddle: String) {
        self.jisYearNumber = jisYearNumber
        self.name = name
        self.nameReading = nameReading
        self.nickname = nickname
        self.uniformNameReading = uniformNameReading
        self.birthdate = birthdate
        self.address = address
        self.deliveryDate = deliveryDate
        self.inquiryNumber = inquiryNumber
        self.licenseColorClassification = licenseColorClassification
        self.validUntil = validUntil
        self.condition1 = condition1
        self.condition2 = condition2
        self.condition3 = condition3
        self.condition4 = condition4
        self.publicSecurityCommitteeName = publicSecurityCommitteeName
        self.licenseNumber = licenseNumber
        self.licenseDate2andSmallandMotorbike = licenseDate2andSmallandMotorbike
        self.licenseDateOthers = licenseDateOthers
        self.licenseDateType2 = licenseDateType2
        self.licenseDateLarge = licenseDateLarge
        self.licenseDateNormal = licenseDateNormal
        self.licenseDateLargeSpecial = licenseDateLargeSpecial
        self.licenseDateLargeBike = licenseDateLargeBike
        self.licenseDateNormalBike = licenseDateNormalBike
        self.licenseDateSmallSpecial = licenseDateSmallSpecial
        self.licenseDateMotorbike = licenseDateMotorbike
        self.licenseDateTowing = licenseDateTowing
        self.licenseDateLarge2 = licenseDateLarge2
        self.licenseDateNormal2 = licenseDateNormal2
        self.licenseDateLargeSpecial2 = licenseDateLargeSpecial2
        self.licenseDateTowing2 = licenseDateTowing2
        self.licenseDateMiddle = licenseDateMiddle
        self.licenseDateMiddle2 = licenseDateMiddle2
        self.licenseDateSemiMiddle = licenseDateSemiMiddle
    }
    
    init(data: Data) throws {
        let reader = JPDriversLicenseRecordDataReader(data: data, offset: 0)
        self.jisYearNumber = try reader.readHex(tag: 0x11)
        self.name = try reader.read0208(tag: 0x12)
        self.nameReading = try reader.read0208(tag: 0x13)
        self.nickname = try reader.read0208(tag: 0x14)
        self.uniformNameReading = try reader.read0208(tag: 0x15)
        self.birthdate = try reader.read0201(tag: 0x16)
        self.address = try reader.read0208(tag: 0x17)
        self.deliveryDate = try reader.read0201(tag: 0x18)
        self.inquiryNumber = try reader.read0201(tag: 0x19)
        self.licenseColorClassification = try reader.read0208(tag: 0x1A)
        self.validUntil = try reader.read0201(tag: 0x1B)
        self.condition1 = try reader.read0208(tag: 0x1C)
        self.condition2 = try reader.read0208(tag: 0x1D)
        self.condition3 = try reader.read0208(tag: 0x1E)
        self.condition4 = try reader.read0208(tag: 0x1F)
        self.publicSecurityCommitteeName = try reader.read0208(tag: 0x20)
        self.licenseNumber = try reader.read0201(tag: 0x21)
        self.licenseDate2andSmallandMotorbike = try reader.read0201(tag: 0x22)
        self.licenseDateOthers = try reader.read0201(tag: 0x23)
        self.licenseDateType2 = try reader.read0201(tag: 0x24)
        self.licenseDateLarge = try reader.read0201(tag: 0x25)
        self.licenseDateNormal = try reader.read0201(tag: 0x26)
        self.licenseDateLargeSpecial = try reader.read0201(tag: 0x27)
        self.licenseDateLargeBike = try reader.read0201(tag: 0x28)
        self.licenseDateNormalBike = try reader.read0201(tag: 0x29)
        self.licenseDateSmallSpecial = try reader.read0201(tag: 0x2A)
        self.licenseDateMotorbike = try reader.read0201(tag: 0x2B)
        self.licenseDateTowing = try reader.read0201(tag: 0x2C)
        self.licenseDateLarge2 = try reader.read0201(tag: 0x2D)
        self.licenseDateNormal2 = try reader.read0201(tag: 0x2E)
        self.licenseDateLargeSpecial2 = try reader.read0201(tag: 0x2F)
        self.licenseDateTowing2 = try reader.read0201(tag: 0x30)
        self.licenseDateMiddle = try reader.read0201(tag: 0x31)
        self.licenseDateMiddle2 = try reader.read0201(tag: 0x32)
        self.licenseDateSemiMiddle = try reader.read0201(tag: 0x33)
    }
}
